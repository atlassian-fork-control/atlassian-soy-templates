package com.atlassian.soy.spring;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WebContextProviderServletFilterTest {
    public static final Locale LOCALE = Locale.CANADA;
    public static final String CONTEXT_PATH = "contextPath";
    private WebContextProviderServletFilter toTest = new WebContextProviderServletFilter();

    final HttpServletRequest request = mock(HttpServletRequest.class);
    final HttpServletResponse response = mock(HttpServletResponse.class);
    final FilterChain filterChain = mock(FilterChain.class);

    @Before
    public void setup() {
        when(response.getLocale()).thenReturn(LOCALE);
        when(request.getContextPath()).thenReturn(CONTEXT_PATH);
    }

    @Test
    public void testHappyPath() throws Exception {
        toTest.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(request, response);
        assertNull(toTest.getRequest());
        assertNull(toTest.getResponse());
    }


    @Test
    public void testFilterChainGetsWebContext() throws Exception {
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                assertEquals(CONTEXT_PATH, toTest.getContextPath());
                assertEquals(LOCALE, toTest.getLocale());
                return null;
            }
        }).when(filterChain).doFilter(request, response);

        toTest.doFilter(request, response, filterChain);

        assertNull(toTest.getRequest());
        assertNull(toTest.getResponse());
    }

    @Test
    public void testChainThrowsWithoutMemoryLeak() throws Exception {
        doThrow(new RuntimeException()).when(filterChain).doFilter(request, response);

        try {
            toTest.doFilter(request, response, filterChain);
        } catch (RuntimeException ignored) {

        }

        assertNull(toTest.getRequest());
        assertNull(toTest.getResponse());
    }

}
