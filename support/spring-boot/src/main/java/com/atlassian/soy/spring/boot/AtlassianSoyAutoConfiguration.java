package com.atlassian.soy.spring.boot;

import com.atlassian.soy.impl.SoyManagerBuilder;
import com.atlassian.soy.impl.functions.ServiceLoaderSoyFunctionSupplier;
import com.atlassian.soy.impl.i18n.ResourceBundleI18nResolver;
import com.atlassian.soy.impl.i18n.WebContextJsLocaleResolver;
import com.atlassian.soy.impl.web.SimpleWebContextProvider;
import com.atlassian.soy.renderer.SoyDataMapper;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.soy.spi.TemplateSetFactory;
import com.atlassian.soy.spi.functions.SoyFunctionSupplier;
import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import com.atlassian.soy.spi.web.WebContextProvider;
import com.atlassian.soy.spring.MessageSourceI18nResolver;
import com.atlassian.soy.spring.ResourceLoaderTemplateSetFactory;
import com.atlassian.soy.spring.SoyTemplateRendererFactoryBean;
import com.atlassian.soy.spring.SpringWebContextProvider;
import com.atlassian.soy.springmvc.EmptyInjectedDataFactory;
import com.atlassian.soy.springmvc.InjectedDataFactory;
import com.atlassian.soy.springmvc.SoyViewResolverFactoryBean;
import com.google.common.base.MoreObjects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Servlet;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Auto-configuration for Atlassian Soy.
 * <p>
 * Spring Boot applications with {@code @EnableAutoConfiguration} with include this module on their classpath
 * will be able to have Atlassian Soy fully configured to use
 *
 * @since 4.2.0
 */
@Configuration
@ConditionalOnClass({SoyTemplateRenderer.class, SoyManagerBuilder.class})
@EnableConfigurationProperties(AtlassianSoySettings.class)
public class AtlassianSoyAutoConfiguration {

    @Autowired(required = false)
    private MessageSource messageSource;
    @Qualifier("soyDataMappers")
    @Autowired(required = false)
    private List<SoyDataMapper<?, ?>> soyDataMappers;
    @Qualifier("fallbackWebContextProvider")
    @Autowired(required = false)
    private WebContextProvider fallbackWebContextProvider;
    @Autowired
    private AtlassianSoySettings settings;
    @Value("${server.servlet.context-path}")
    private String contextPath;

    @Bean
    public SoyTemplateRendererFactoryBean soyTemplateRenderer(WebContextProvider webContextProvider,
                                                              TemplateSetFactory templateSetFactory,
                                                              JsLocaleResolver jsLocaleResolver,
                                                              I18nResolver i18nResolver,
                                                              SoyFunctionSupplier functionSupplier) {
        SoyTemplateRendererFactoryBean factoryBean = new SoyTemplateRendererFactoryBean();
        factoryBean.setWebContextProvider(webContextProvider);
        factoryBean.setTemplateSetFactory(templateSetFactory);
        factoryBean.setJsLocaleResolver(jsLocaleResolver);
        factoryBean.setI18nResolver(i18nResolver);
        factoryBean.setSoyDataMappers(soyDataMappers == null ? Collections.emptyList() : soyDataMappers);
        factoryBean.setSoyFunctionSupplier(functionSupplier);
        return factoryBean;
    }

    @ConditionalOnMissingBean(WebContextProvider.class)
    @Bean
    public WebContextProvider webContextProvider() {
        return new SpringWebContextProvider(fallbackWebContextProvider == null ?
            new SimpleWebContextProvider(MoreObjects.firstNonNull(settings.getFallbackContextPath(), contextPath)) :
            fallbackWebContextProvider);
    }

    @ConditionalOnMissingBean(TemplateSetFactory.class)
    @Bean
    public TemplateSetFactory templateSetFactory() {
        return new ResourceLoaderTemplateSetFactory(Arrays.asList(settings.getTemplateLocations().split(",")));
    }

    @ConditionalOnMissingBean(JsLocaleResolver.class)
    @Bean
    public JsLocaleResolver jsLocaleResolver(WebContextProvider webContextProvider) {
        return new WebContextJsLocaleResolver(webContextProvider);
    }

    @ConditionalOnMissingBean(I18nResolver.class)
    @Bean
    public I18nResolver i18nResolver(WebContextProvider webContextProvider) {
        return messageSource == null ?
                new ResourceBundleI18nResolver(webContextProvider) :
                new MessageSourceI18nResolver(webContextProvider, messageSource);
    }

    @ConditionalOnMissingBean(SoyFunctionSupplier.class)
    @Bean
    public SoyFunctionSupplier soyFunctionSupplier() {
        return new ServiceLoaderSoyFunctionSupplier();
    }

    @Configuration
    @ConditionalOnClass(Servlet.class)
    @ConditionalOnWebApplication
    @AutoConfigureAfter(ServletWebServerFactoryAutoConfiguration.class)
    public static class SoyWebAutoConfiguration {
        @ConditionalOnMissingBean(name = "soyViewResolver")
        @Bean
        public SoyViewResolverFactoryBean soyViewResolver(InjectedDataFactory injectedDataFactory,
                                                          SoyTemplateRenderer soyTemplateRenderer) {
            SoyViewResolverFactoryBean factoryBean = new SoyViewResolverFactoryBean();
            factoryBean.setInjectedDataFactory(injectedDataFactory);
            factoryBean.setSoyTemplateRenderer(soyTemplateRenderer);
            return factoryBean;
        }

        @ConditionalOnMissingBean(InjectedDataFactory.class)
        @Bean
        public InjectedDataFactory injectedDataFactory() {
            return new EmptyInjectedDataFactory();
        }
    }
}
