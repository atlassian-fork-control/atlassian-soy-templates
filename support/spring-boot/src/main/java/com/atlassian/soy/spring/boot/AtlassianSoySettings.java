package com.atlassian.soy.spring.boot;

import com.atlassian.soy.spring.ResourceLoaderTemplateSetFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "atlassian.soy")
public class AtlassianSoySettings {

    /**
     * A comma separated list of URI glob patterns referencing the location of the Soy template files
     */
    private String templateLocations = ResourceLoaderTemplateSetFactory.DEFAULT_FILE_PATTERN;

    /**
     * The context path to use for the {@code contextPath()} function if there is no current request
     */
    private String fallbackContextPath;

    public String getTemplateLocations() {
        return templateLocations;
    }

    public void setTemplateLocations(String templateLocations) {
        this.templateLocations = templateLocations;
    }

    public String getFallbackContextPath() {
        return fallbackContextPath;
    }

    public void setFallbackContextPath(String fallbackContextPath) {
        this.fallbackContextPath = fallbackContextPath;
    }
}
