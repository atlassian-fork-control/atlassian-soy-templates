package com.atlassian.soy.cli;

import com.atlassian.soy.spi.i18n.I18nResolver;
import com.google.common.collect.ImmutableList;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;

/**
 * {@link I18nResolver} implementation that reads values from a single properties file
 */
class FileBasedI18nResolver implements I18nResolver {
    private List<PropertyResourceBundle> propertiesResources;

    FileBasedI18nResolver(List<RelativePath> paths) throws IOException {
        ImmutableList.Builder<PropertyResourceBundle> builder = ImmutableList.builder();
        for (RelativePath path : paths) {
            builder.add(readPropertiesFile(path.absolutePath.getAbsolutePath()));
        }
        propertiesResources = builder.build();
    }

    @Override
    public String getText(Locale locale, String key) {
        return new MessageFormat(getRawText(locale, key)).format(new Serializable[0]);
    }

    @Override
    public String getRawText(Locale locale, final String key) {
        for (PropertyResourceBundle propertiesResource : propertiesResources) {

            if (propertiesResource.containsKey(key)) {
                return propertiesResource.getString(key);
            }
        }
        return key;
    }

    @Override
    public String getText(String key) {
        return getText(null, key);
    }

    @Override
    public String getText(String key, Serializable... arguments) {
        return new MessageFormat(getRawText(null, key)).format(arguments);
    }

    private PropertyResourceBundle readPropertiesFile(String propertiesFile) throws IOException {
        try (FileInputStream fis = new FileInputStream(propertiesFile)) {
            return new PropertyResourceBundle(fis);
        }
    }
}
