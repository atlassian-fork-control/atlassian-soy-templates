package com.atlassian.soy.cli;

import com.atlassian.soy.renderer.SoyException;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * CLI to for rendering soy or compiling soy to js.
 */
public class Main {
    public static void main(String... args) throws IOException, ParseException, SoyException {
        final String minimumJavaVersion = "1.7";
        final String javaVersion = System.getProperty("java.version");

        if (javaVersion.compareTo(minimumJavaVersion) < 0) {
            System.out.println("Java version " + minimumJavaVersion + " or higher required to run. Current version is " + javaVersion);
            System.exit(1);
            return;
        }

        CommandLineParser parser = new GnuParser();
        Options options = constructOptions();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar atlassian-soy-cli-support.jar", options, true);
            System.exit(1);
            return;
        }
        String type = cmd.getOptionValue("type");
        String baseDirectory = cmd.getOptionValue("basedir");
        String i18nFiles = cmd.getOptionValue("i18n");
        String i18nBaseDirectory = cmd.getOptionValue("i18n-basedir", baseDirectory);
        String include = cmd.getOptionValue("glob");
        String exclude = cmd.getOptionValue("exclude");
        String outputDirectory = cmd.getOptionValue("outdir");
        String outputExtension = cmd.getOptionValue("extension", "js".equals(type) ? "js" : "html");
        String dependencies = cmd.getOptionValue("dependencies", "");
        String rootNamespace = cmd.getOptionValue("rootnamespace", "");
        String[] functionLocations = cmd.getOptionValues("functions");
        String data = cmd.getOptionValue("data", "");
        String contextPath = cmd.getOptionValue("context-path");
        boolean useAjsContextPath = cmd.hasOption("use-ajs-context-path");
        boolean useAjsI18n = cmd.hasOption("use-ajs-i18n");

        run(type, i18nBaseDirectory, i18nFiles, baseDirectory, include, exclude, outputDirectory, outputExtension,
                dependencies, rootNamespace, data, functionLocations,
                contextPath, useAjsContextPath, useAjsI18n);
    }

    static void run(final String type, final String i18nBaseDirectory, final String i18nFiles,
                    final String baseDirectory, final String glob, final String exclude,
                    final String outputDirectory, final String outputExtension, final String dependencies,
                    final String rootNamespace, final String data, final String[] functionLocations,
                    final String contextPath, final boolean useAjsContextPath, final boolean useAjsI18n)
            throws IOException, SoyException, ParseException {
        checkNotNull(baseDirectory, "can't find any files to compile unless a base directory is specified");
        checkNotNull(glob, "can't find any soy files unless you provide a glob to match them");
        checkNotNull(outputDirectory, "won't get any output unless you specify an output directory");

        final ClassLoader soyFunctionClassLoader = createClassLoader(functionLocations);
        final PathGlob i18nFileGlob = new PathGlob(i18nBaseDirectory, i18nFiles);
        final PathGlob source = new PathGlob(baseDirectory, glob, exclude);
        if ("render".equals(type)) {
            new SoyRenderer().run(soyFunctionClassLoader, rootNamespace, i18nFileGlob, source, toGlobs(dependencies),
                    outputDirectory, outputExtension, asMap(data), contextPath);
        } else if ("js".equals(type)) {
            new SoyJsCompiler().run(soyFunctionClassLoader, i18nFileGlob, source, outputDirectory, outputExtension,
                    contextPath, useAjsContextPath, useAjsI18n);
        } else {
            throw new ParseException("Invalid type: " + type);
        }
    }

    private static Iterable<PathGlob> toGlobs(String input) throws ParseException {
        return Iterables.transform(asMap(input).entrySet(), new Function<Map.Entry<String, String>, PathGlob>() {
            @Override
            public PathGlob apply(Map.Entry<String, String> input) {
                return new PathGlob(input.getKey(), input.getValue());
            }
        });
    }

    private static Map<String, String> asMap(String input) throws ParseException {
        Map<String, String> map = new LinkedHashMap<>();
        if (null != input && input.length() > 0) {
            for (String part : input.split(",")) {
                int i = part.indexOf(":");
                if (-1 == i) {
                    throw new ParseException("Error parsing token: " + part + ", expected :");
                }
                map.put(part.substring(0, i), part.substring(i + 1));
            }
        }
        return map;
    }

    private static Options constructOptions() {
        final Options options = new Options();
        options.addOption(required("t", "type", true, "type, either 'render' to render or 'js' to compile to js"));
        options.addOption(required("b", "basedir", true, "base directory for soy source files"));
        options.addOption(required("g", "glob", true, "glob for matching soy files"));
        options.addOption(required("o", "outdir", true, "directory to output files to"));
        options.addOption("e", "exclude", true, "glob for excluding soy files");
        options.addOption("i", "i18n", true, "glob for matching i18n properties file");
        options.addOption("ib", "i18n-basedir", true, "base directory for i18n properties file");
        Option functionsOption = new Option("f", "functions", true, "locations of custom soy functions, usually a jar");
        functionsOption.setArgs(Option.UNLIMITED_VALUES);
        options.addOption(functionsOption);
        options.addOption("p", "dependencies", true, "soy dependencies (soy rendering only) in the form <basePath1>:<glob1>,<basePath2>:<glob2>");
        options.addOption("x", "extension", true, "output extension");
        options.addOption("d", "data", true, "data to pass to soy renderer (soy rendering only) in the form <key1>:<value1>,<key2>:<value2>");
        options.addOption("r", "rootnamespace", true, "root soy namespace (soy rendering only)");
        options.addOption("cp", "context-path", true, "The context path to use");
        options.addOption("acp", "use-ajs-context-path", false, "AJS.contextPath() is used instead of a static context path (soy js only)");
        options.addOption("atext", "use-ajs-i18n", false, "AJS.I18n.getText() is used instead of resolving from i18n properties file");
        return options;
    }

    private static Option required(String opt, String longOpt, boolean hasArg, String description) {
        Option option = new Option(opt, longOpt, hasArg, description);
        option.setRequired(true);
        return option;
    }

    private static ClassLoader createClassLoader(String[] functionLocations) throws MalformedURLException {
        if (functionLocations == null || functionLocations.length == 0) {
            return Main.class.getClassLoader();
        }

        URI uri = Paths.get(System.getProperty("user.dir")).toAbsolutePath().toUri();
        URL[] urls = new URL[functionLocations.length];
        for (int i = 0; i < functionLocations.length; i++) {
            urls[i] = uri.resolve(functionLocations[i]).toURL();
        }

        return new URLClassLoader(urls, Main.class.getClassLoader());
    }

}
