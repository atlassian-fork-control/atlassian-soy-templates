package com.atlassian.soy.cli;

import com.atlassian.soy.impl.SimpleTemplateSetFactory;
import com.atlassian.soy.impl.SoyManager;
import com.atlassian.soy.impl.SoyManagerBuilder;
import com.atlassian.soy.impl.functions.ServiceLoaderSoyFunctionSupplier;
import com.atlassian.soy.impl.web.SimpleWebContextProvider;
import com.atlassian.soy.renderer.SoyException;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Renders soy
 */
class SoyRenderer {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    void run(ClassLoader soyFunctionClassLoader, String rootNamespace, PathGlob i18nFileGlob, PathGlob source,
             Iterable<PathGlob> dependencies, String outputDirectory, String outputExtension,
             Map<String, String> data, String contextPath) throws IOException, SoyException {
        log.info("Compiling soy templates to HTML");

        List<RelativePath> paths = FileFinder.findFiles(source);
        List<RelativePath> i18nPaths = FileFinder.findFiles(i18nFileGlob);

        SoyManagerBuilder builder = new SoyManagerBuilder()
                .templateSetFactory(new SimpleTemplateSetFactory(allDependencies(source, dependencies)))
                .i18nResolver(new FileBasedI18nResolver(i18nPaths))
                .functionSupplier(new ServiceLoaderSoyFunctionSupplier(soyFunctionClassLoader));

        if (contextPath != null) {
            builder = builder.webContextProvider(new SimpleWebContextProvider(contextPath));
        }

        SoyManager soyManager = builder.build();

        for (RelativePath path : paths) {
            String relativePath = path.relativePath;
            String relativePathNoExtension = getPathNoExtension(relativePath);
            String templateId = rootNamespace + "." + createTemplateId(relativePathNoExtension);
            File outputFile = new File(outputDirectory, relativePathNoExtension + "." + outputExtension);

            File outputFileParent = outputFile.getParentFile();
            if (outputFileParent.mkdirs()) {
                log.info("Created directory: " + outputFileParent);
            }

            log.info("Compiling soy file " + relativePath + " to " + outputFile);

            try (Writer writer = Files.newWriter(outputFile, Charset.forName("UTF-8"))) {
                soyManager.render(writer, "", templateId, Collections.<String, Object>unmodifiableMap(data), Collections.<String, Object>emptyMap());
            }
        }
    }

    private Set<URL> allDependencies(final PathGlob source, final Iterable<PathGlob> dependencies) throws IOException {
        List<File> soyDeps = new ArrayList<>();
        for (PathGlob dependency : dependencies) {
            soyDeps.addAll(allFilesInPath(dependency));
        }
        soyDeps.addAll(allFilesInPath(source));

        Set<URL> urls = new HashSet<>();
        for (File file : soyDeps) {
            if (file != null) {
                urls.add(file.toURI().toURL());
            }
        }
        return urls;
    }

    private List<File> allFilesInPath(final PathGlob path) throws IOException {
        List<RelativePath> dependencyPaths = FileFinder.findFiles(path);
        return Lists.transform(dependencyPaths, new Function<RelativePath, File>() {
            @Override
            public File apply(@Nullable RelativePath input) {
                return (null == input) ? null : input.absolutePath;
            }
        });
    }

    private static String getPathNoExtension(String path) {
        int index = path.lastIndexOf(".");
        if (-1 == index) {
            return path;
        }
        return path.substring(0, index);
    }

    // creates a template ID based on a path
    private static String createTemplateId(String path) {
        return path.replace(File.separator, ".");
    }
}
