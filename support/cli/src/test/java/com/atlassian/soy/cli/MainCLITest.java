package com.atlassian.soy.cli;

import org.apache.commons.lang3.SystemUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeFalse;

public class MainCLITest {
    private Path baseDir;
    private Path outputDir;
    private Path i18nFile;

    @Before
    public void setup() throws IOException {
        baseDir = Files.createTempDirectory("my-input");
        outputDir = Files.createTempDirectory("my-output");
        i18nFile = Files.createTempFile("i18n", ".properties");
    }

    @Test
    public void testRequiresBaseDir() throws Exception {
        try {
            Main.run("render", baseDir.toString(), null, null, null, null, null, null, null, null, null, null, null, false, false);
            fail("Should have complained about not having a base directory");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), containsString("base directory"));
        }
    }

    @Test
    public void testCustomFunction() throws Exception {
        Path soy = Files.createTempFile(baseDir, "template", ".soy");
        String templateName = nameOf(soy);
        String namespace = "all.of.these.templates";
        Files.write(soy, templateWithCustomFunction(namespace, templateName).getBytes());

        //new String[]{"com.atlassian.soy.cli.CustomSoyFunction"}

        Main.run("render", baseDir.toString(), i18nFile.toString(), baseDir.toString(),
                "**.soy", null, outputDir.toString(), "html", null, namespace, null, null, null, false, false);

        Path result = outputDir.resolve(templateName + ".html");
        List<String> contents = Files.readAllLines(result, Charset.defaultCharset());

        assertThat(contents, hasItem(containsString("<p>Hello world! bar</p>")));
    }

    @Test
    public void testToStringFunctionIsAvailable() throws Exception {
        Path soy = Files.createTempFile(baseDir, "template", ".soy");
        String templateName = nameOf(soy);
        String namespace = "to.string.template";
        Files.write(soy, getToStringTemplate(namespace, templateName).getBytes());

        Main.run("js", baseDir.toString(), i18nFile.toString(), baseDir.toString(), "**.soy", null, outputDir.toString(), "soy.js", null, namespace, null, null,
                "/my/context/path", false, false);

        Path result = outputDir.resolve(templateName + ".soy.js");
        List<String> contents = Files.readAllLines(result, Charset.defaultCharset());

        assertThat(contents, hasItem(containsString("return '' + soy.$$escapeHtml('' + 'toString this');")));
    }

    @Test
    public void testRenderModeWillOutputHtmlForTemplateWithSameNameAsSoyFile() throws Exception {
        Path soy = Files.createTempFile(baseDir, "template", ".soy");
        String templateName = nameOf(soy);
        String namespace = "all.of.these.templates";
        Files.write(soy, basicTemplate(namespace, templateName).getBytes());


        Main.run("render", baseDir.toString(), i18nFile.toString(), baseDir.toString(), "**.soy", null, outputDir.toString(), "html", null, namespace, null, null, null, false, false);

        Path result = outputDir.resolve(templateName + ".html");
        List<String> contents = Files.readAllLines(result, Charset.defaultCharset());

        assertThat(contents, hasItem(containsString("<p>Hello world!</p>")));
    }

    @Test
    public void testRenderModeWithDependentTemplatesInDifferentNamespaces() throws Exception {
        //Main.asMap, called by toGlobs when dependencies are provided, uses an approach to split directories
        //from globs which does not work on Windows, where : can appear in a path
        assumeFalse(SystemUtils.IS_OS_WINDOWS);

        Path soy = Files.createTempFile(baseDir, "template", ".soy");
        String templateName = nameOf(soy);
        String namespace = "dependent.on.other.templates";
        Files.write(soy, dependentTemplate(namespace, templateName).getBytes());

        String depsString = "";

        Path dep1 = Paths.get(getClass().getResource("/dep1.soy").toURI());
        depsString += dep1.getParent().toString();
        depsString += ":*.soy";

        Path dep2 = Paths.get(getClass().getResource("/more/dep2.soy").toURI());
        depsString += ",";
        depsString += dep2.getParent().toString();
        depsString += ":*.soy";

        Main.run("render", baseDir.toString(), i18nFile.toString(), baseDir.toString(), "**.soy", null, outputDir.toString(), "html", depsString, namespace, null, null, null, false, false);

        Path result = outputDir.resolve(templateName + ".html");
        List<String> contents = Files.readAllLines(result, Charset.defaultCharset());

        assertThat(contents, hasItem(containsString("Hello, <b>John</b>")));
        assertThat(contents, hasItem(containsString("Pleasure to meet ya")));
    }

    @Test
    public void testRenderModeWithCustomizedContextPath() throws Exception {
        Path soy = Files.createTempFile(baseDir, "template", ".soy");
        String templateName = nameOf(soy);
        String namespace = "all.of.these.templates";
        Files.write(soy, contextPathTemplate(namespace, templateName).getBytes());

        Main.run("render", baseDir.toString(), i18nFile.toString(), baseDir.toString(), "**.soy", null, outputDir.toString(), "html", null, namespace, null, null,
                "/my/context/path", false, false);

        Path result = outputDir.resolve(templateName + ".html");
        List<String> contents = Files.readAllLines(result, Charset.defaultCharset());

        assertThat(contents, hasItem(containsString("<a href=\"/my/context/path\">Link me</a>")));
    }

    @Test
    public void testJsModeWithCustomizedContextPath() throws Exception {
        Path soy = Files.createTempFile(baseDir, "template", ".soy");
        String templateName = nameOf(soy);
        String namespace = "all.of.these.templates";
        Files.write(soy, contextPathTemplate(namespace, templateName).getBytes());

        Main.run("js", baseDir.toString(), i18nFile.toString(), baseDir.toString(), "**.soy", null, outputDir.toString(), "soy.js", null, namespace, null, null,
                "/my/context/path", false, false);

        Path result = outputDir.resolve(templateName + ".soy.js");
        List<String> contents = Files.readAllLines(result, Charset.defaultCharset());

        assertThat(contents, hasItem(containsString("<a href=\"' + soy.$$escapeHtml(\"/my/context/path\") + '\">Link me</a>")));
    }

    @Test
    public void testJsModeWithUseAjsContextPath() throws Exception {
        Path soy = Files.createTempFile(baseDir, "template", ".soy");
        String templateName = nameOf(soy);
        String namespace = "all.of.these.templates";
        Files.write(soy, contextPathTemplate(namespace, templateName).getBytes());

        Main.run("js", baseDir.toString(), i18nFile.toString(), baseDir.toString(), "**.soy", null, outputDir.toString(), "soy.js", null, namespace, null, null,
                "/my/context/path", true, false);

        Path result = outputDir.resolve(templateName + ".soy.js");
        List<String> contents = Files.readAllLines(result, Charset.defaultCharset());

        assertThat(contents, hasItem(containsString("<a href=\"' + soy.$$escapeHtml(AJS.contextPath()) + '\">Link me</a>")));
    }

    @Test
    public void testJsModeWithUseI18n() throws Exception {
        Path soy = Files.createTempFile(baseDir, "template", ".soy");
        String templateName = nameOf(soy);
        String namespace = "all.of.these.templates";
        Files.write(soy, getTextTemplate(namespace, templateName).getBytes());

        Main.run("js", baseDir.toString(), i18nFile.toString(), baseDir.toString(), "**.soy", null, outputDir.toString(), "soy.js", null, namespace, null, null,
                "/my/context/path", false, true);

        Path result = outputDir.resolve(templateName + ".soy.js");
        List<String> contents = Files.readAllLines(result, Charset.defaultCharset());

        assertThat(contents, hasItem(containsString("AJS.I18n.getText('some.key','a','b')")));
    }

    private String nameOf(Path path) {
        String name = path.getFileName().toString();
        return name.substring(0, name.lastIndexOf("."));
    }

    private String templateWithCustomFunction(final String namespace, final String templateName) {
        return ""
                + "{namespace " + namespace + "}\n\n"
                + "/**\n"
                + " * A template!\n"
                + " */\n"
                + "{template ." + templateName + "}\n"
                + "<p>Hello world! {foo()}</p>\n"
                + "{/template}\n";
    }

    private String basicTemplate(final String namespace, final String templateName) {
        return ""
                + "{namespace " + namespace + "}\n\n"
                + "/**\n"
                + " * A template!\n"
                + " */\n"
                + "{template ." + templateName + "}\n"
                + "<p>Hello world!</p>\n"
                + "{/template}\n"
                ;
    }

    private String dependentTemplate(final String namespace, final String templateName) {
        return ""
                + "{namespace " + namespace + "}\n\n"
                + "/**\n"
                + " * A template!\n"
                + " */\n"
                + "{template ." + templateName + "}\n"
                + "    {call some.deps.greet}\n"
                + "        {param name: 'John'/}\n"
                + "    {/call}\n"
                + "    <p>\n"
                + "        {call some.more.deps.meet /}\n"
                + "    </p>\n"
                + "{/template}\n"
                ;
    }

    private String contextPathTemplate(final String namespace, final String templateName) {
        return ""
                + "{namespace " + namespace + "}\n\n"
                + "/**\n"
                + " * A template!\n"
                + " */\n"
                + "{template ." + templateName + "}\n"
                + "<a href=\"{contextPath()}\">Link me</a>\n"
                + "{/template}\n"
                ;
    }

    private String getTextTemplate(final String namespace, final String templateName) {
        return ""
                + "{namespace " + namespace + "}\n\n"
                + "/**\n"
                + " * A template!\n"
                + " */\n"
                + "{template ." + templateName + "}\n"
                + "{getText('some.key', 'a', 'b')}\n"
                + "{/template}\n"
                ;
    }

    private String getToStringTemplate(final String namespace, final String templateName) {
        return ""
                + "{namespace " + namespace + "}\n\n"
                + "/**\n"
                + " * A template!\n"
                + " */\n"
                + "{template ." + templateName + "}\n"
                + "{toString('toString this')}\n"
                + "{/template}\n"
                ;
    }
}
