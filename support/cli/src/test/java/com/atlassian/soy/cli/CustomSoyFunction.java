package com.atlassian.soy.cli;

import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class CustomSoyFunction implements SoyServerFunction<String>, SoyClientFunction {
    @Override
    public JsExpression generate(JsExpression... args) {
        return new JsExpression("'bar'");
    }

    @Override
    public String getName() {
        return "foo";
    }

    @Override
    public String apply(Object... args) {
        return "bar";
    }

    @Override
    public Set<Integer> validArgSizes() {
        return ImmutableSet.of(0);
    }
}
