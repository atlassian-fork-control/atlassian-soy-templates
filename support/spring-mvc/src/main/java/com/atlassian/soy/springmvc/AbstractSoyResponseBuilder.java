package com.atlassian.soy.springmvc;

import com.atlassian.soy.springmvc.errors.DetailedError;
import com.atlassian.soy.springmvc.errors.WebErrorUtils;
import com.google.common.collect.Maps;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractSoyResponseBuilder<B extends AbstractSoyResponseBuilder<B>> {
    protected final Object view;
    protected final Map<String, Object> model = new HashMap<>();

    private static final String DATA_ATTRIBUTES = "dataAttributes";

    public AbstractSoyResponseBuilder(String viewName) {
        this.view = viewName;
    }

    public AbstractSoyResponseBuilder(View view) {
        this.view = view;
    }

    protected abstract B self();

    public Map<String, String> getDataAttributes() {
        if (model.containsKey(DATA_ATTRIBUTES)) {
            //noinspection unchecked
            return (Map<String, String>) model.get(DATA_ATTRIBUTES);
        }
        Map<String, String> attributes = Maps.newHashMap();
        model.put(DATA_ATTRIBUTES, attributes);
        return attributes;
    }

    public B put(String key, Object value) {
        model.put(key, value);
        return self();
    }

    public B putIf(boolean condition, String key, Object value) {
        if (condition) {
            model.put(key, value);
        }
        return self();
    }

    public B putValidationErrors(Errors errors) {
        model.put("fieldErrors", WebErrorUtils.toFieldErrorMap(errors.getFieldErrors()));
        model.put("formErrors", WebErrorUtils.toFormErrors(errors.getGlobalErrors()));
        return self();
    }

    public B putDetailedErrors(Iterable<DetailedError> detailedErrors) {
        model.put("detailedErrors", detailedErrors);
        return self();
    }

    public B putFieldErrors(Map<String, ? extends Iterable<String>> fieldErrors) {
        model.put("fieldErrors", fieldErrors);
        return self();
    }

    public B putFormErrors(String... errors) {
        return putFormErrors(Arrays.asList(errors));
    }

    public B putFormErrors(Iterable<String> errors) {
        model.put("formErrors", errors);
        return self();
    }

    public B putAll(Map<String, Object> map) {
        model.putAll(map);
        return self();
    }

    public ModelAndView build() {
        return
                view instanceof String
                        ? new ModelAndView((String) view, model)
                        : new ModelAndView((View) view, model);

    }
}
