package com.atlassian.soy.springmvc.errors;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Convenience web error functions.
 *
 * @since 2.3
 */
public class WebErrorUtils {

    protected WebErrorUtils() {
        throw new UnsupportedOperationException(getClass().getName() +
                " is a utility class and should not be instantiated");
    }

    @SuppressWarnings("unchecked")
    public static Map<String, ? extends Iterable<String>> toFieldErrorMap(Iterable<FieldError> fieldErrors) {
        final Map<String, List<String>> errors = new HashMap<>();
        for (FieldError fieldError : fieldErrors) {
            if (!errors.containsKey(fieldError.getField())) {
                errors.put(fieldError.getField(), Lists.<String>newArrayList());
            }
            errors.get(fieldError.getField()).add(fieldError.getDefaultMessage());
        }
        return errors;
    }

    public static Collection<String> toFormErrors(List<ObjectError> fieldErrors) {
        return new ArrayList<>(Lists.transform(fieldErrors, new Function<ObjectError, String>() {

            @Override
            public String apply(ObjectError error) {
                return error.getDefaultMessage();
            }
        }));
    }
}
