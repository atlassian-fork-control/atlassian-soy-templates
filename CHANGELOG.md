# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- Missing plugin modules are now a soft warning when resolving the dependency graph for template compilation (SOY-115)

## [4.5.0] - 2017-02-10
### Added
- Soy template compilations are shared between usage in plugin modules (SOY-134)

[Unreleased]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/master..soy-templates-parent-4.5.0 
[4.5.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-4.5.0..soy-templates-parent-4.4.3
