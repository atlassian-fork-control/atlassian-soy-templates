package com.atlassian.soy.test;

import com.atlassian.soy.springmvc.SoyResponseBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/sanity")
public class SanityController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showSanityPage() {
        return new SoyResponseBuilder("sanity.sanityTest")
                .build();
    }

}
