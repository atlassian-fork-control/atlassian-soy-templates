package it.com.atlassian.soy.test;

import com.google.common.io.CharStreams;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.assertEquals;

public class SanityStandaloneTest {

    private String contextPath;

    @Before
    public void setUp() {
        contextPath = System.getProperty("context.path");
    }

    @Test
    public void testSanityPage() throws Exception {
        String url = "http://localhost:8080" + contextPath + "/sanity.html";

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.connect();
        assertEquals(200, connection.getResponseCode());

        String encoding = connection.getContentEncoding();
        try (Reader reader = (encoding == null) ?
                new InputStreamReader(connection.getInputStream()) :
                new InputStreamReader(connection.getInputStream(), encoding)) {
            String body = CharStreams.toString(reader);
            assertEquals(
                    "<!DOCTYPE html>" +
                            "<html lang=\"en\">" +
                            "<head>" +
                            "<meta charset=\"utf-8\" />" +
                            "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EDGE\">" +
                            "<title>You&#39;re on " + contextPath + "</title>" +
                            "</head>" +
                            "<body>Hello, Slim Shady!</body>" +
                            "</html>",
                    body
            );
        }
    }

}
