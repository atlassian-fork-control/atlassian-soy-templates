package it.com.atlassian.soy.test;

import com.google.common.io.CharStreams;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.assertEquals;

public class SanityPluginTest {

    private String baseUrl;
    private String contextPath;

    @Before
    public void setUp() throws Exception {
        baseUrl = System.getProperty("baseurl");
        contextPath = System.getProperty("context.path");

    }

    @Test
    public void testSanityPage() throws Exception {
        final HttpURLConnection connection = (HttpURLConnection) new URL(baseUrl + "/plugins/servlet/soy-sanity").openConnection();
        connection.connect();
        assertEquals(200, connection.getResponseCode());
        String encoding = connection.getContentEncoding();
        try (Reader reader = (encoding == null) ?
                new InputStreamReader(connection.getInputStream()) :
                new InputStreamReader(connection.getInputStream(), encoding)) {
            String body = CharStreams.toString(reader);
            assertEquals(
                    "<!DOCTYPE html>" +
                            "<html lang=\"en\">" +
                            "<head>" +
                            "<meta charset=\"utf-8\" />" +
                            "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EDGE\">" +
                            "<title>You&#39;re on " + contextPath + "</title>" +
                            "</head>" +
                            "<body>Hello, Slim Shady!</body>" +
                            "</html>",
                    body
            );
        }
    }
}
