package com.atlassian.soy.renderer;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A representation of sanitized content which does not need to be escaped for a particular {@link #getType() type}
 * of content.
 * <p>
 * Implementers of {@link SoyServerFunction} can use this as their return type to indicate to the soy renderer that
 * the content does not need to be auto escaped. If no {@link #getType() type} is provided,
 * the value will be treated as HTML content. If the value is written with the different context of a different type,
 * it will still be escaped.
 *
 * @since 2.6
 */
@PublicApi
public class SanitizedString {

    private final String value;
    private final SanitizationType type;

    public SanitizedString(@Nonnull String value) {
        this(value, SanitizationType.HTML);
    }

    public SanitizedString(@Nonnull String value, @Nonnull SanitizationType type) {
        this.value = checkNotNull(value, "value");
        this.type = checkNotNull(type, "type");
    }

    @Nonnull
    public SanitizationType getType() {
        return type;
    }

    @Nonnull
    public String getValue() {
        return value;
    }
}
