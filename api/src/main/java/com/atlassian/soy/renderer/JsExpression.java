package com.atlassian.soy.renderer;

import com.atlassian.annotations.PublicApi;

/**
 * @since 1.1
 */
@PublicApi
public class JsExpression {

    private final String text;

    public JsExpression(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
