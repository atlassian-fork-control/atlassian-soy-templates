package com.atlassian.soy.renderer;

import com.atlassian.annotations.PublicApi;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a class as requiring a custom mapper to convert it to SoyData. The value of the annotation should be the
 * same as the value returned by {@link com.atlassian.soy.renderer.SoyDataMapper#getName()} on the appropriate mapper.
 *
 * @since 1.1
 */
@PublicApi
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CustomSoyDataMapper {
    String value();
}
