package com.atlassian.soy.renderer;


import com.atlassian.annotations.PublicSpi;

/**
 * An soy function which can be invoked on the client.
 * <p>
 * Note, {@link SoyClientFunction soy client functions} should be pure functions, meaning
 * that for a given set of inputs they should always return the same result regardless of
 * the state of the system and with no side effects. Any impure function <strong>must</strong>
 * implement {@code com.atlassian.soy.renderer.StatefulSoyClientFunction},
 * encode the state in the URL and provide all dimensions ({@code com.atlassian.webresource.api.prebake.Dimensions}),
 * i.e., states it can assume.
 * This is so that the URL changes when the state does, ensuring
 * clients are not using stale resources.
 * <p>
 * Any query parameters passed into the URL builder will be available via {@link QueryParamsResolver}
 * on invocation of the function.
 *
 * @since 1.1
 */
@PublicSpi
public interface SoyClientFunction extends SoyFunction {
    /**
     * Generates a JavasScript expression from the given JavaScript expressions.
     * <p>
     * This is differs from {@link SoyServerFunction} as it deals with runtime values where as this
     * method deals with JavaScript expressions at template compile time
     *
     * @param args the args for the function. The number of args is guaranteed
     *             to be one of valid arg sizes supplied by {@link #validArgSizes()}
     * @return a JavaScript expression
     */
    JsExpression generate(JsExpression... args);
}
