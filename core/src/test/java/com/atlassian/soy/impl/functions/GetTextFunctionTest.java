package com.atlassian.soy.impl.functions;

import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.restricted.StringData;
import com.google.template.soy.jssrc.restricted.JsExpr;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GetTextFunctionTest {
    private final static Locale mockLocale = Locale.CHINESE;

    @Mock
    private JsLocaleResolver jsLocaleResolver;
    @Mock
    private I18nResolver i18nResolver;

    @Test
    public void testComputeForTofu() throws Exception {
        GetTextFunction function = new GetTextFunction(jsLocaleResolver, i18nResolver, false);
        doReturn(mockLocale).when(jsLocaleResolver).getLocale();
        doReturn("").when(i18nResolver).getText(anyString(), (Serializable[]) Matchers.<Serializable>anyVararg());

        function.computeForJava(Arrays.<SoyValue>asList(StringData.forValue("ma.key"), StringData.forValue("blah")));
        verify(i18nResolver).getText("ma.key", new String[]{"blah"});
    }

    @Test
    public void testComputeForJsEscapes() throws Exception {
        GetTextFunction function = new GetTextFunction(jsLocaleResolver, i18nResolver, false);
        doReturn(mockLocale).when(jsLocaleResolver).getLocale();
        doReturn("<strong>escaped strong</strong>").when(i18nResolver).getText(eq(mockLocale), eq("ma.key"));
        JsExpr result = function.computeForJsSrc(Arrays.asList(new JsExpr("'ma.key'", 0)));
        assertEquals("Expected i18n string to be js escaped",
                "'\\x3cstrong\\x3eescaped strong\\x3c/strong\\x3e'", result.getText());
    }

    @Test
    public void testComputeForJsNoArgs() throws Exception {
        GetTextFunction function = new GetTextFunction(jsLocaleResolver, i18nResolver, false);
        doReturn(mockLocale).when(jsLocaleResolver).getLocale();
        doReturn("some text").when(i18nResolver).getText(eq(mockLocale), eq("ma.key"));

        JsExpr result = function.computeForJsSrc(Arrays.asList(new JsExpr("'ma.key'", 0)));
        verify(i18nResolver).getText(mockLocale, "ma.key");
        assertEquals("\'some text\'", result.getText());
    }

    @Test
    public void testComputeForJsWithI18nArgs() throws Exception {
        GetTextFunction function = new GetTextFunction(jsLocaleResolver, i18nResolver, false);
        doReturn(mockLocale).when(jsLocaleResolver).getLocale();
        doReturn("some text").when(i18nResolver).getRawText(eq(mockLocale), eq("ma.key"));
        JsExpr result = function.computeForJsSrc(Arrays.asList(new JsExpr("'ma.key'", 0), new JsExpr("'blah'", 0)));
        verify(i18nResolver).getRawText(mockLocale, "ma.key");
        assertEquals("AJS.format('some text','blah')", result.getText());
    }

    @Test
    public void testComputeForAJSI18nWithNoArgs() throws Exception {
        GetTextFunction function = new GetTextFunction(jsLocaleResolver, i18nResolver, true);
        doReturn(mockLocale).when(jsLocaleResolver).getLocale();
        JsExpr result = function.computeForJsSrc(Arrays.asList(new JsExpr("'ma.key'", 0)));
        assertEquals("AJS.I18n.getText('ma.key')", result.getText());
    }

    @Test
    public void testComputeForAJSI18nWithArgs() throws Exception {
        GetTextFunction function = new GetTextFunction(jsLocaleResolver, i18nResolver, true);
        doReturn(mockLocale).when(jsLocaleResolver).getLocale();
        JsExpr result = function.computeForJsSrc(Arrays.asList(new JsExpr("'ma.key'", 0), new JsExpr("'blah'", 0)));
        assertEquals("AJS.I18n.getText('ma.key','blah')", result.getText());
    }

}
