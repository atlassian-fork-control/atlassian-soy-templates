package com.atlassian.soy.impl.data;

import com.google.common.collect.Sets;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class IntrospectorJavaBeanAccessorResolverTest {

    private IntrospectorJavaBeanAccessorResolver resolver = new IntrospectorJavaBeanAccessorResolver();

    @Test
    public void testResolveAccessors() throws Exception {
        Map<String, Method> methods = resolver.resolveAccessors(Jelly.class);
        assertEquals(Sets.newHashSet("tasty", "color"), methods.keySet());
        assertEquals(Boolean.FALSE, methods.get("tasty").invoke(new Jelly()));
    }

    @Test
    public void testResolveAccessorsOnPrivateClass() throws Exception {
        SuperVillain villain = new TheJoker();
        Map<String, Method> methods = resolver.resolveAccessors(villain.getClass());
        assertEquals(Sets.newHashSet("name"), methods.keySet());
        Method method = methods.get("name");
        assertNotNull("name method should be not null", method);
        assertTrue("method should be public", Modifier.isPublic(method.getModifiers()));
        assertTrue("declaring class should be public", Modifier.isPublic(method.getDeclaringClass().getModifiers()));
        assertEquals(villain.getName(), method.invoke(villain));
    }

    public static class Jelly {

        private boolean m_tasty;
        private boolean m_secret = true;
        private String m_color;
        private String m_prefix;

        public boolean isTasty() {
            return m_tasty;
        }

        private boolean isSecret() {
            return m_secret;
        }

        public String getColor() {
            return m_color;
        }

        public void setPrefix(String value) {
            m_prefix = value;
        }

        public String toString() {
            return m_prefix + m_color + "tasty=" + m_tasty;
        }

    }

    public static interface SuperVillain {
        String getName();
    }

    private static class TheJoker implements SuperVillain {
        @Override
        public String getName() {
            return "The Joker";
        }
    }

}