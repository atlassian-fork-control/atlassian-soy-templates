package com.atlassian.soy.impl.functions;

import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyListData;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.restricted.IntegerData;
import com.google.template.soy.data.restricted.StringData;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IsListFunctionTest {
    private IsListFunction toTest = new IsListFunction();

    @Test
    public void testEmptyList() {
        assertTrue(toTest.computeForJava(Collections.<SoyValue>singletonList(new SoyListData())).booleanValue());
    }

    @Test
    public void testNonEmptyList() {
        SoyData soyData = toTest.computeForJava(Collections.<SoyValue>singletonList(new SoyListData("Hello world")));
        assertTrue(soyData.booleanValue());

        soyData = toTest.computeForJava(Collections.<SoyValue>singletonList(new SoyListData("Hello world", "Goodbye world")));
        assertTrue(soyData.booleanValue());
    }

    @Test
    public void testInvalidTypes() {
        assertFalse(toTest.computeForJava(Collections.<SoyValue>singletonList(StringData.EMPTY_STRING)).booleanValue());
        assertFalse(toTest.computeForJava(Collections.<SoyValue>singletonList(StringData.forValue("Hello world"))).booleanValue());
        assertFalse(toTest.computeForJava(Collections.<SoyValue>singletonList(IntegerData.forValue(100))).booleanValue());
        assertFalse(toTest.computeForJava(Collections.<SoyValue>singletonList(new SoyMapData())).booleanValue());
        assertFalse(toTest.computeForJava(Collections.<SoyValue>singletonList(new SoyMapData("Hello", "world"))).booleanValue());
    }
}
