package com.atlassian.soy.impl.functions;

import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import com.google.template.soy.data.SanitizedContent;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.restricted.StringData;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.shared.restricted.Sanitizers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GetTextAsHtmlFunctionTest {
    private final static Locale mockLocale = Locale.CHINESE;

    @Mock
    private JsLocaleResolver jsLocaleResolver;
    @Mock
    private I18nResolver i18nResolver;

    private GetTextAsHtmlFunction function;

    @Before
    public void setUp() throws Exception {
        function = new GetTextAsHtmlFunction(jsLocaleResolver, i18nResolver, false);
    }

    @Test
    public void testComputeForTofu() throws Exception {
        doReturn("").when(i18nResolver).getText(anyString(), Matchers.<Serializable[]>anyVararg());

        SoyData result = function.computeForJava(Arrays.<SoyValue>asList(
                StringData.forValue("<ma.key>"),
                StringData.forValue("<blah.value>")));
        verify(i18nResolver).getText("<ma.key>",
                new String[]{Sanitizers.escapeHtml("<blah.value>")});
        assertTrue(result instanceof SanitizedContent);
        assertEquals(((SanitizedContent) result).getContentKind(),
                SanitizedContent.ContentKind.HTML);
    }

    @Test
    public void testForTofuWithMissingKey() throws Exception {
        doReturn("").when(i18nResolver).getText(anyString(), Matchers.<Serializable[]>anyVararg());

        final String getTextKey = "<ma.key";
        doReturn(getTextKey).when(i18nResolver).getText(eq(getTextKey),
                (Serializable[]) Matchers.<Serializable>anyVararg());
        SoyData result = function.computeForJava(Arrays.<SoyValue>asList(
                StringData.forValue(getTextKey), StringData.forValue("")));
        assertEquals(Sanitizers.escapeHtml(getTextKey), result.toString());
    }

    @Test
    public void testComputeForJsNoArgs() throws Exception {
        doReturn(mockLocale).when(jsLocaleResolver).getLocale();
        doReturn("").when(i18nResolver).getText(eq(mockLocale), anyString());

        JsExpr result = function.computeForJsSrc(Arrays.asList(
                new JsExpr("'<ma.key>'", 0)));
        verify(i18nResolver).getText(mockLocale, "<ma.key>");
        assertTrue(result.getText().startsWith(("soydata.VERY_UNSAFE.ordainSanitizedHtml")));
    }

    @Test
    public void testComputeForJsWithI18nArgs() throws Exception {
        doReturn(mockLocale).when(jsLocaleResolver).getLocale();
        doReturn("").when(i18nResolver).getRawText(eq(mockLocale), anyString());

        JsExpr result = function.computeForJsSrc(Arrays.asList(
                new JsExpr("'<ma.key>'", 0),
                new JsExpr("'<blah.value>'", 0)));
        verify(i18nResolver).getRawText(mockLocale, "<ma.key>");
        assertTrue(result.getText().startsWith(("soydata.VERY_UNSAFE.ordainSanitizedHtml")));
    }

    @Test
    public void testComputeForChoiceFormat() throws Exception {
        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                String message = "{0} {1,choice,-1#is negative|0#is zero or fraction|1#is one|1.0<is 1+|2#is two|2<is more than 2.}";
                Object[] args = invocation.getArguments();
                // Mockito auto expands varargs into the arguments array. Total PITA
                args = Arrays.copyOfRange(args, 1, args.length);
                return MessageFormat.format(message, args);
            }
        })
                .when(i18nResolver).getText(anyString(), Matchers.<Serializable[]>anyVararg());

        SoyData result = function.computeForJava(Arrays.<SoyValue>asList(
                StringData.forValue("<ma.key>"),
                StringData.forValue("<blah.value>"),
                SoyData.createFromExistingData(2)));
        assertTrue(result instanceof SanitizedContent);
        SanitizedContent content = (SanitizedContent) result;
        assertEquals(content.getContentKind(), SanitizedContent.ContentKind.HTML);
        assertEquals("&lt;blah.value&gt; is two", content.getContent());
    }

}
