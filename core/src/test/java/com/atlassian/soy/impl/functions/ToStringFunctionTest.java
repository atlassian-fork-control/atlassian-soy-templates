package com.atlassian.soy.impl.functions;

import com.atlassian.soy.renderer.JsExpression;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ToStringFunctionTest {
    private ToStringFunction function = new ToStringFunction();

    @Test
    public void testGenerate() throws Exception {
        assertEquals("'' + variable", function.generate(new JsExpression("variable")).getText());
    }

    @Test
    public void testApply() throws Exception {
        assertEquals("null", function.apply(new Object[]{null}));
        assertEquals("blah", function.apply("blah"));
        assertEquals("Snowflake", function.apply(new CustomObject()));
    }

    private static class CustomObject {

        @Override
        public String toString() {
            return "Snowflake";
        }
    }
}