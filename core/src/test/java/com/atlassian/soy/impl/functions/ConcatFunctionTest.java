package com.atlassian.soy.impl.functions;

import com.google.common.collect.Lists;
import com.google.template.soy.data.SoyDict;
import com.google.template.soy.data.SoyList;
import com.google.template.soy.data.SoyListData;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.SoyValueHelper;
import com.google.template.soy.data.restricted.StringData;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConcatFunctionTest {
    private ConcatFunction toTest = new ConcatFunction(SoyValueHelper.UNCUSTOMIZED_INSTANCE);

    private static final int ABC_VALUE = 123;
    private static final int DEF_VALUE = 456;
    private static final int JKL_VALUE = 876;
    private static final int GHI_VALUE = 789;

    private static final StringData ABC = StringData.forValue("ABC");
    private static final StringData DEF = StringData.forValue("DEF");
    private static final StringData GHI = StringData.forValue("GHI");
    private static final StringData JKL = StringData.forValue("JKL");

    @Test(expected = IllegalArgumentException.class)
    public void testTypeIncompatability() {
        toTest.computeForJava(Lists.<SoyValue>newArrayList(new SoyListData(), new SoyMapData()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTypeIncompatabilityMapFirst() {
        toTest.computeForJava(Lists.<SoyValue>newArrayList(new SoyMapData(), new SoyListData()));
    }

    @Test
    public void testEmptyBoth() {
        final SoyValue concatedEmptyLists = toTest.computeForJava(Lists.<SoyValue>newArrayList(new SoyListData(), new SoyListData()));
        assertTrue(((SoyList) concatedEmptyLists).asResolvedJavaList().isEmpty());
        final SoyValue concatedEmptyMaps = toTest.computeForJava(Lists.<SoyValue>newArrayList(new SoyMapData(), new SoyMapData()));
        assertTrue(((SoyDict) concatedEmptyMaps).asResolvedJavaStringMap().isEmpty());
    }

    @Test
    public void testEmptyOnLeft() {
        final SoyValue concatedLists = toTest.computeForJava(Lists.<SoyValue>newArrayList(new SoyListData(), new SoyListData(ABC, DEF)));

        final List<? extends SoyValue> listData = ((SoyList) concatedLists).asResolvedJavaList();
        assertEquals(2, listData.size());
        assertEquals(ABC, listData.get(0));
        assertEquals(DEF, listData.get(1));

        final SoyValue concatedMaps = toTest.computeForJava(Lists.<SoyValue>newArrayList(new SoyMapData(), new SoyMapData(ABC.stringValue(), ABC_VALUE, DEF.stringValue(), DEF_VALUE)));

        final Map<String, ? extends SoyValue> mapData = ((SoyDict) concatedMaps).asResolvedJavaStringMap();
        assertEquals(2, mapData.size());
        assertEquals(ABC_VALUE, mapData.get(ABC.stringValue()).integerValue());
        assertEquals(DEF_VALUE, mapData.get(DEF.stringValue()).integerValue());

    }

    @Test
    public void testEmptyOnRight() {
        final SoyValue concatedLists = toTest.computeForJava(Lists.<SoyValue>newArrayList(new SoyListData(GHI, JKL), new SoyListData()));

        final List<? extends SoyValue> listData = ((SoyList) concatedLists).asResolvedJavaList();
        assertEquals(2, listData.size());
        assertEquals(GHI, listData.get(0));
        assertEquals(JKL, listData.get(1));

        final SoyValue concatedMaps = toTest.computeForJava(Lists.<SoyValue>newArrayList(new SoyMapData(GHI.stringValue(), GHI_VALUE, JKL.stringValue(), JKL_VALUE), new SoyMapData()));

        final Map<String, ? extends SoyValue> mapData = ((SoyDict) concatedMaps).asResolvedJavaStringMap();
        assertEquals(2, mapData.size());
        assertEquals(GHI_VALUE, mapData.get(GHI.stringValue()).integerValue());
        assertEquals(JKL_VALUE, mapData.get(JKL.stringValue()).integerValue());

    }

    @Test
    public void testNonEmptyOnBoth() {

        final SoyValue concatedLists = toTest.computeForJava(Lists.<SoyValue>newArrayList(new SoyListData(ABC, DEF), new SoyListData(GHI, JKL)));

        final List<? extends SoyValue> listData = ((SoyList) concatedLists).asResolvedJavaList();
        assertEquals(4, listData.size());
        assertEquals(ABC, listData.get(0));
        assertEquals(DEF, listData.get(1));
        assertEquals(GHI, listData.get(2));
        assertEquals(JKL, listData.get(3));

        final SoyValue concatedMaps = toTest.computeForJava(Lists.<SoyValue>newArrayList(new SoyMapData(ABC.stringValue(), ABC_VALUE, DEF.stringValue(), DEF_VALUE), new SoyMapData(GHI.stringValue(), GHI_VALUE, JKL.stringValue(), JKL_VALUE)));

        final Map<String, ? extends SoyValue> mapData = ((SoyDict) concatedMaps).asResolvedJavaStringMap();
        assertEquals(4, mapData.size());
        assertEquals(ABC_VALUE, mapData.get(ABC.stringValue()).integerValue());
        assertEquals(DEF_VALUE, mapData.get(DEF.stringValue()).integerValue());
        assertEquals(GHI_VALUE, mapData.get(GHI.stringValue()).integerValue());
        assertEquals(JKL_VALUE, mapData.get(JKL.stringValue()).integerValue());
    }

}
