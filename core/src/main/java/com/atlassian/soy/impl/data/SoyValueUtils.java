package com.atlassian.soy.impl.data;

import com.atlassian.soy.renderer.SanitizationType;
import com.atlassian.soy.renderer.SanitizedString;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.template.soy.data.SanitizedContent;
import com.google.template.soy.data.SoyDict;
import com.google.template.soy.data.SoyList;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.restricted.BooleanData;
import com.google.template.soy.data.restricted.FloatData;
import com.google.template.soy.data.restricted.IntegerData;
import com.google.template.soy.data.restricted.NullData;
import com.google.template.soy.data.restricted.StringData;
import com.google.template.soy.data.restricted.UndefinedData;

import java.util.ArrayList;
import java.util.HashMap;

public class SoyValueUtils {

    private SoyValueUtils() {
        throw new UnsupportedOperationException();
    }

    public static Object fromSoyValue(SoyValue data) {
        if (data == NullData.INSTANCE || data == UndefinedData.INSTANCE) {
            return null;
        } else if (data instanceof JavaBeanSoyDict) {
            return ((JavaBeanSoyDict) data).getDelegate();
        } else if (data instanceof EnumSoyValue) {
            return ((EnumSoyValue) data).getValue();
        } else if (data instanceof SoyDict) {
            return new HashMap<>(Maps.transformValues(((SoyDict) data).asResolvedJavaStringMap(), new Function<SoyValue, Object>() {
                @Override
                public Object apply(SoyValue from) {
                    return fromSoyValue(from);
                }
            }));
        } else if (data instanceof SoyList) {
            return new ArrayList<>(Lists.transform(((SoyList) data).asResolvedJavaList(), new Function<SoyValue, Object>() {
                @Override
                public Object apply(SoyValue from) {
                    return fromSoyValue(from);
                }
            }));
        } else if (data instanceof StringData) {
            return data.stringValue();
        } else if (data instanceof SanitizedContent) {
            SanitizedContent sanitizedContent = (SanitizedContent) data;
            return new SanitizedString(
                    sanitizedContent.getContent(),
                    toSanitizationType(sanitizedContent.getContentKind())
            );
        } else if (data instanceof IntegerData) {
            return ((IntegerData) data).getValue();
        } else if (data instanceof BooleanData) {
            return ((BooleanData) data).getValue();
        } else if (data instanceof FloatData) {
            return ((FloatData) data).getValue();
        } else {
            return data.stringValue();
        }
    }

    private static SanitizationType toSanitizationType(SanitizedContent.ContentKind kind) {
        switch (kind) {
            case CSS:
                return SanitizationType.CSS;
            case JS:
                return SanitizationType.JS;
            case JS_STR_CHARS:
                return SanitizationType.JS_STRING;
            case HTML:
                return SanitizationType.HTML;
            case ATTRIBUTES:
                return SanitizationType.HTML_ATTRIBUTE;
            case TEXT:
                return SanitizationType.TEXT;
            case URI:
                return SanitizationType.URI;
            default:
                throw new UnsupportedOperationException("Unsupported kind " + kind);
        }
    }

}
