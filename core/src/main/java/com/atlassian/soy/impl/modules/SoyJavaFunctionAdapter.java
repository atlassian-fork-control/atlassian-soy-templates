package com.atlassian.soy.impl.modules;

import com.atlassian.soy.impl.data.SoyValueUtils;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.SoyValueConverter;
import com.google.template.soy.data.SoyValueHelper;
import com.google.template.soy.shared.restricted.SoyJavaFunction;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

class SoyJavaFunctionAdapter implements SoyJavaFunction {

    private final SoyServerFunction<?> soyServerFunction;

    private SoyValueConverter converter;

    public SoyJavaFunctionAdapter(SoyServerFunction<?> soyServerFunction) {
        this.soyServerFunction = soyServerFunction;
    }

    @Override
    public SoyValue computeForJava(List<SoyValue> args) {
        final Object[] pluginArgs = Lists.transform(args, new Function<SoyValue, Object>() {
            public Object apply(SoyValue from) {
                return SoyValueUtils.fromSoyValue(from);
            }
        }).toArray();

        Object returnValue = soyServerFunction.apply(pluginArgs);
        return converter.convert(returnValue).resolve();
    }

    @Override
    public String getName() {
        return soyServerFunction.getName();
    }

    @Override
    public Set<Integer> getValidArgsSizes() {
        return soyServerFunction.validArgSizes();
    }

    @Inject
    void setConverter(SoyValueHelper converter) {
        this.converter = converter;
    }

}
