package com.atlassian.soy.impl.functions;

import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import com.google.inject.Singleton;
import com.google.template.soy.data.SanitizedContent;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.UnsafeSanitizedContentOrdainer;
import com.google.template.soy.data.restricted.NumberData;
import com.google.template.soy.data.restricted.StringData;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.jssrc.restricted.SoyJsSrcFunction;
import com.google.template.soy.shared.restricted.Sanitizers;
import com.google.template.soy.shared.restricted.SoyJavaFunction;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * I18N getTextAsHtml soy function.
 *
 * @since 2.2
 */
@Singleton
public class GetTextAsHtmlFunction implements SoyJsSrcFunction, SoyJavaFunction {
    public static final String FUNCTION_NAME = "getTextAsHtml";

    private final GetTextFunction getTextFunction;

    @Inject
    public GetTextAsHtmlFunction(JsLocaleResolver jsLocaleResolver, I18nResolver i18nResolver, @Named(SoyProperties.USE_AJS_I18N) boolean useAjsI18n) {
        this.getTextFunction = new GetTextFunction(jsLocaleResolver, i18nResolver, useAjsI18n);
    }

    @Override
    public String getName() {
        return FUNCTION_NAME;
    }

    @Override
    public Set<Integer> getValidArgsSizes() {
        return getTextFunction.getValidArgsSizes();
    }

    @Override
    public JsExpr computeForJsSrc(List<JsExpr> args) {
        List<JsExpr> escapedArgs = new ArrayList<>(args.size());
        // The first argument is a string literal of the i18n key. it needs to be left
        // as is
        escapedArgs.add(args.get(0));
        for (JsExpr arg : args.subList(1, args.size())) {
            escapedArgs.add(new JsExpr("typeof (" + arg.getText() + ") === 'number' ? " + arg.getText() + " : soy.$$escapeHtml(" +
                    arg.getText() + ")", Integer.MAX_VALUE));
        }
        return new JsExpr("soydata.VERY_UNSAFE.ordainSanitizedHtml(" +
                getTextFunction.computeForJsSrc(escapedArgs).getText() + ")",
                Integer.MAX_VALUE);
    }

    @Override
    public SoyData computeForJava(List<SoyValue> args) {
        ArrayList<SoyValue> escapedArgs = new ArrayList<>();
        SoyValue i18nKey = args.get(0);
        escapedArgs.add(i18nKey);
        for (SoyValue arg : args.subList(1, args.size())) {
            SoyValue escapedArg = arg instanceof NumberData ?
                    arg :
                    StringData.forValue(Sanitizers.escapeHtml(arg));
            escapedArgs.add(escapedArg);
        }

        String html = getTextFunction.computeForJava(escapedArgs).stringValue();
        // If the i18n key was resolved. Escape it just to be sure
        if (html.equals(i18nKey.stringValue())) {
            html = Sanitizers.escapeHtml(html);
        }
        return UnsafeSanitizedContentOrdainer.ordainAsSafe(html, SanitizedContent.ContentKind.HTML);

    }
}
