package com.atlassian.soy.impl.functions;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Singleton;
import com.google.template.soy.data.SoyDict;
import com.google.template.soy.data.SoyEasyDict;
import com.google.template.soy.data.SoyEasyList;
import com.google.template.soy.data.SoyList;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.SoyValueHelper;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.jssrc.restricted.SoyJsSrcFunction;
import com.google.template.soy.shared.restricted.SoyJavaFunction;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

/**
 * Soy util for concatenating Java iterables/JS arrays or Java maps/JS objects.
 *
 * @since 2.3
 */
@Singleton
public class ConcatFunction implements SoyJsSrcFunction, SoyJavaFunction {
    public static final String FUNCTION_NAME = "concat";

    private final SoyValueHelper soyValueHelper;

    @Inject
    public ConcatFunction(SoyValueHelper soyValueHelper) {
        this.soyValueHelper = soyValueHelper;
    }


    public String getName() {
        return FUNCTION_NAME;
    }

    @Override
    public Set<Integer> getValidArgsSizes() {
        return ImmutableSet.of(2);
    }

    @Override
    public JsExpr computeForJsSrc(List<JsExpr> args) {
        JsExpr listA = args.get(0);
        JsExpr listB = args.get(1);
        return new JsExpr("atl_soy.concat(" + listA.getText() + ", " + listB.getText() + ")", Integer.MAX_VALUE);
    }

    @Override
    public SoyValue computeForJava(List<SoyValue> args) {

        SoyValue first = args.get(0);
        SoyValue second = args.get(1);
        if (first instanceof SoyList && second instanceof SoyList) {
            return concatIterables((SoyList) first, (SoyList) second);
        }

        if (first instanceof SoyDict && second instanceof SoyDict) {
            return concatMaps((SoyDict) first, (SoyDict) second);
        }

        throw new IllegalArgumentException("concat() accepts two arguments that are either both Maps or both Iterables.");
    }

    private SoyList concatIterables(SoyList first, SoyList second) {
        SoyEasyList list = soyValueHelper.newEasyList();
        list.addAllFromList(first);
        list.addAllFromList(second);

        return list.makeImmutable();
    }

    private SoyDict concatMaps(SoyDict first, SoyDict second) {
        SoyEasyDict dict = soyValueHelper.newEasyDict();
        dict.setItemsFromDict(first);
        dict.setItemsFromDict(second);

        return dict.makeImmutable();
    }
}
