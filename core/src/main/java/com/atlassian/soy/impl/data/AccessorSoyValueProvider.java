package com.atlassian.soy.impl.data;

import com.atlassian.soy.renderer.SoyException;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.SoyValueConverter;
import com.google.template.soy.data.SoyValueProvider;

import java.lang.reflect.Method;
import javax.annotation.Nonnull;

class AccessorSoyValueProvider implements SoyValueProvider {

    private final SoyValueConverter converter;
    private final Method accessorMethod;
    private final Object delegate;

    private volatile SoyValue resolvedValue;

    public AccessorSoyValueProvider(SoyValueConverter converter, Object delegate, Method accessorMethod) {
        this.converter = converter;
        this.delegate = delegate;
        this.accessorMethod = accessorMethod;
    }

    @Nonnull
    @Override
    public SoyValue resolve() {
        SoyValue resolvedValue = this.resolvedValue;
        if (resolvedValue == null) {
            synchronized (this) {
                resolvedValue = this.resolvedValue;
                if (resolvedValue == null) {
                    resolvedValue = this.resolvedValue = doResolve();
                }
            }
        }

        return resolvedValue;
    }

    private SoyValue doResolve() {
        Object resolvedValue;
        try {
            resolvedValue = accessorMethod.invoke(delegate);
        } catch (Exception e) {
            throw new SoyException("Failed to invoke accessor " + accessorMethod.getName() + " on instance of " + delegate.getClass(), e);
        }

        return converter.convert(resolvedValue).resolve();
    }

    @Override
    public boolean equals(@Nonnull SoyValueProvider o) {
        if (!(o instanceof AccessorSoyValueProvider)) {
            return false;
        }

        AccessorSoyValueProvider other = (AccessorSoyValueProvider) o;

        return accessorMethod.equals(other.accessorMethod) && delegate.equals(other.delegate);
    }

}
