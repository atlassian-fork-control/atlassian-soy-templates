package com.atlassian.soy.impl;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.google.common.base.Supplier;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import io.atlassian.util.concurrent.ResettableLazyReference;

/**
 * Creates and Caches Injectors for different sets of additional SoyFunctions that might be required by different
 * plugin module dependencies.
 */
class SoyDependencyInjectorFactory {
    /**
     * Injectors are relatively expensive and synchronous to create. Don't do it unless you have to. It needs to be
     * reset on plugin enabling / disabling as it includes plugin defined global functions.
     */
    @TenantAware(value = TenancyScope.TENANTLESS,
            comment = "SOY modules and dependencies needed for SOY Compiler, same for all tenants.")
    private final ResettableLazyReference<Injector> defaultInjectorRef;

    public SoyDependencyInjectorFactory(final Supplier<Iterable<Module>> moduleSupplier) {
        this.defaultInjectorRef = new ResettableLazyReference<Injector>() {
            @Override
            protected Injector create() {
                return Guice.createInjector(moduleSupplier.get());
            }
        };
    }

    public void clear() {
        defaultInjectorRef.reset();
    }

    /**
     * Gets the Injector for creating Soy objects.
     */
    public Injector get() {
        return defaultInjectorRef.get();
    }
}