package com.atlassian.soy.impl.data;

import com.atlassian.soy.renderer.SoyException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

public class IntrospectorJavaBeanAccessorResolver implements JavaBeanAccessorResolver {

    private static final Set<String> BANNED_PROPERTY_NAMES = ImmutableSet.of("class", "classLoader");

    @Override
    public void clearCaches() {
        Introspector.flushCaches();
    }

    @Override
    public Map<String, Method> resolveAccessors(Class<?> targetClass) {
        checkNotNull(targetClass, "targetClass");

        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(targetClass);
            PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
            if (descriptors == null) {
                return Collections.emptyMap();
            }

            ImmutableMap.Builder<String, Method> builder = ImmutableMap.builder();
            for (PropertyDescriptor descriptor : descriptors) {
                String propertyName = descriptor.getName();
                Method readMethod = getPublicMethod(descriptor.getReadMethod());
                if (readMethod != null && !BANNED_PROPERTY_NAMES.contains(propertyName)) {
                    builder.put(propertyName, readMethod);
                }
            }
            return builder.build();
        } catch (IntrospectionException e) {
            throw new SoyException("Failed to introspect class " + targetClass.getName(), e);
        }
    }

    private Method getPublicMethod(Method method) {
        if (method == null) {
            return null;
        }

        if (!Modifier.isPublic(method.getModifiers())) {
            return null;
        }


        Class<?> declaringClass = method.getDeclaringClass();

        if (Modifier.isPublic(declaringClass.getModifiers())) {
            return method;
        }

        return findPublicMethodOnInterfaces(declaringClass, method.getName(), method.getParameterTypes());
    }

    private Method findPublicMethodOnInterfaces(Class<?> targetClass, String name, Class<?>... parameterTypes) {
        while (targetClass != null) {
            for (Class<?> interfaceClass : targetClass.getInterfaces()) {
                if (!Modifier.isPublic(interfaceClass.getModifiers())) {
                    continue;
                }

                try {
                    return interfaceClass.getDeclaredMethod(name, parameterTypes);
                } catch (NoSuchMethodException e) {
                    // ignore
                }

                Method method = findPublicMethodOnInterfaces(interfaceClass, name, parameterTypes);
                if (method != null) {
                    return null;
                }

            }

            targetClass = targetClass.getSuperclass();
        }

        return null;
    }


}
