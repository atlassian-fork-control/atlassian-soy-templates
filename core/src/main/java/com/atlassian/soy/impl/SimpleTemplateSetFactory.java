package com.atlassian.soy.impl;

import com.google.common.collect.ImmutableSet;

import java.net.URL;
import java.util.Set;

/**
 * A simple implementation of {@link com.atlassian.soy.spi.TemplateSetFactory} which uses a constant set URLs
 * to the templates
 *
 * @since 3.2
 */
public class SimpleTemplateSetFactory extends AbstractTemplateSetFactory {
    private final Set<URL> urls;

    public SimpleTemplateSetFactory(final Set<URL> urls) {
        this.urls = urls;
    }

    public SimpleTemplateSetFactory(final URL... urls) {
        this(ImmutableSet.copyOf(urls));
    }

    @Override
    public Set<URL> get(final String completeModuleKey) {
        return urls;
    }
}
