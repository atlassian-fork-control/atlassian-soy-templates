package com.atlassian.soy.spi.functions;

import com.atlassian.soy.renderer.SoyFunction;
import com.google.common.base.Supplier;

/**
 * A supplier of function soy functions
 *
 * @since 2.4
 */
public interface SoyFunctionSupplier extends Supplier<Iterable<SoyFunction>> {

    /**
     * @return an iterable of custom soy functions
     */
    Iterable<SoyFunction> get();

}
