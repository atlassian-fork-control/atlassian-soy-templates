package com.atlassian.soy.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.soy.impl.data.JavaBeanAccessorResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.spi.modules.GuiceModuleSupplier;
import com.google.common.collect.Lists;
import com.google.template.soy.tofu.SoyTofuException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.matchers.ThrowableMessageMatcher;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PluginDefaultSoyManagerTest {
    private static final Map<String,Object> none = Collections.<String, Object>emptyMap();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private ServletContextFactory servletContextFactory;
    @Mock
    private GuiceModuleSupplier moduleSupplier;
    @Mock
    private JavaBeanAccessorResolver accessorResolver;

    @Mock private WebResourceModuleDescriptor entrypointWithNoDeps;
    @Mock private WebResourceModuleDescriptor entrypointWithDeps;
    @Mock private WebResourceModuleDescriptor entrypointWithIncompleteDeps;
    @Mock private WebResourceModuleDescriptor entrypointWithMissingDeps;

    @Mock private ResourceDescriptor one;
    @Mock private ResourceDescriptor two;

    private SoyManager manager;
    private StringWriter writer;

    @Before
    public void setup() throws IOException {
        Plugin plugin = mock(Plugin.class);

        // Use our test project's template files
        ClassLoader loader = getClass().getClassLoader();
        when(plugin.getResource("one.soy")).thenReturn(loader.getResource("one.soy"));
        when(plugin.getResource("two.soy")).thenReturn(loader.getResource("two.soy"));
        when(one.getLocation()).thenReturn("one.soy");
        when(two.getLocation()).thenReturn("two.soy");

        // Set up our web-resource descriptors to reference various files.
        // (1) a standalone soy template web-resource
        when(entrypointWithNoDeps.getCompleteKey()).thenReturn("enabledPlugin:entrypoint-with-no-deps");
        when(entrypointWithNoDeps.getPlugin()).thenReturn(plugin);
        when(entrypointWithNoDeps.getResourceDescriptors()).thenReturn(Lists.newArrayList(one));

        // (2) a soy template that depends on the template in our standalone web-resource
        when(entrypointWithDeps.getCompleteKey()).thenReturn("enabledPlugin:entrypoint-with-deps");
        when(entrypointWithDeps.getPlugin()).thenReturn(plugin);
        when(entrypointWithDeps.getResourceDescriptors()).thenReturn(Lists.newArrayList(two));
        when(entrypointWithDeps.getDependencies()).thenReturn(Lists.newArrayList("enabledPlugin:entrypoint-with-no-deps"));

        // (3) this template depends on our standalone, but the dependency wasn't declared
        when(entrypointWithIncompleteDeps.getCompleteKey()).thenReturn("enabledPlugin:entrypoint-with-incomplete-deps");
        when(entrypointWithIncompleteDeps.getPlugin()).thenReturn(plugin);
        when(entrypointWithIncompleteDeps.getResourceDescriptors()).thenReturn(Lists.newArrayList(two));

        // (4) this template is standalone, so should render, no matter how badly configured the dependency graph is
        when(entrypointWithMissingDeps.getCompleteKey()).thenReturn("enabledPlugin:entrypoint-with-missing");
        when(entrypointWithMissingDeps.getPlugin()).thenReturn(plugin);
        when(entrypointWithMissingDeps.getResourceDescriptors()).thenReturn(Lists.newArrayList(one));
        when(entrypointWithMissingDeps.getDependencies()).thenReturn(Lists.newArrayList("this.does.not:exist", "enabledPlugin:disabledModule"));

        // Register our web-resources
        when(pluginAccessor.getEnabledPluginModule("enabledPlugin:disabledModule")).thenReturn(null);
        when(pluginAccessor.getEnabledPluginModule(entrypointWithNoDeps.getCompleteKey())).thenReturn((ModuleDescriptor)entrypointWithNoDeps);
        when(pluginAccessor.getEnabledPluginModule(entrypointWithDeps.getCompleteKey())).thenReturn((ModuleDescriptor)entrypointWithDeps);
        when(pluginAccessor.getEnabledPluginModule(entrypointWithIncompleteDeps.getCompleteKey())).thenReturn((ModuleDescriptor)entrypointWithIncompleteDeps);
        when(pluginAccessor.getEnabledPluginModule(entrypointWithMissingDeps.getCompleteKey())).thenReturn((ModuleDescriptor)entrypointWithMissingDeps);

        // Set up the remainder of our required things
        final WebResourceTemplateSetFactory templateSetFactory = new WebResourceTemplateSetFactory(pluginAccessor, servletContextFactory);
        manager = new DefaultSoyManager(moduleSupplier, accessorResolver, templateSetFactory);
        writer = new StringWriter();
    }

    // SOY-16 SOY-29 Failing to handle disabled modules meant we threw whatever unchecked exception guice wanted to
    @Test
    public void disabledModuleKeyTriggersSoyException() throws SoyException {
        System.setProperty("atlassian.dev.mode", "false");
        expectedException.expect(SoyException.class);
        expectedException.expectMessage("Unable to compile Soy templates at: enabledPlugin:disabledModule");
        expectedException.expectCause(new ThrowableMessageMatcher<>(containsString("Must have non-zero number of input Soy files.")));
        manager.render(writer, "enabledPlugin:disabledModule", "myTemplate", none, none);
    }

    // SOY-29
    @Test
    public void disabledModuleKeyInDevModeAlsoTriggersSoyException() throws SoyException {
        System.setProperty("atlassian.dev.mode", "true");
        expectedException.expect(SoyException.class);
        expectedException.expectMessage("Unable to compile Soy templates at: enabledPlugin:disabledModule");
        expectedException.expectCause(new ThrowableMessageMatcher<>(containsString("Must have non-zero number of input Soy files.")));
        manager.render(writer, "enabledPlugin:disabledModule", "myTemplate", none, none);
    }

    // SOY-0
    @Test
    public void standaloneModulesCanBeCompiledAndUsed() throws SoyException {
        manager.render(writer, entrypointWithNoDeps.getCompleteKey(), "soytest.templates.one", none, none);
        assertThat(writer.toString(), equalTo("Hello, world!"));
    }

    // SOY-115
    @Test
    public void dependencyGraphWithMissingModuleDoesNotBreakCompilationOrUsage() throws SoyException {
        manager.render(writer, entrypointWithMissingDeps.getCompleteKey(), "soytest.templates.one", none, none);
        assertThat(writer.toString(), equalTo("Hello, world!"));
    }

    // SOY-0
    @Test
    public void interdependentModulesCanBeCompiledAndUsed() throws SoyException {
        manager.render(writer, entrypointWithDeps.getCompleteKey(), "soytest.templates.two", none, none);
        assertThat(writer.toString(), equalTo("Hello, world! And hello again!"));
    }

    // SOY-0
    @Test
    public void modulesWithMissingTemplateDependenciesWillCompileButThrowWhenUsed() throws SoyException {
        expectedException.expect(SoyTofuException.class);
        expectedException.expectMessage("Attempting to render undefined template 'soytest.templates.one'.");
        manager.render(writer, entrypointWithIncompleteDeps.getCompleteKey(), "soytest.templates.two", none, none);
    }

    @After
    public void clearDevMode() {
        System.clearProperty("atlassian.dev.mode");
    }
}
