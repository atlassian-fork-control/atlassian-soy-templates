package com.atlassian.soy.impl.i18n;

import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.soy.impl.functions.LocaleUtils;
import com.atlassian.soy.renderer.QueryParamsResolver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class QueryParamsJsLocaleResolverTest {
    @Mock
    QueryParams queryParams;
    @Mock
    QueryParamsResolver queryParamsResolver;

    @Test
    public void testValidLocale() {
        when(queryParams.get(QueryParamsJsLocaleResolver.QUERY_KEY)).thenReturn(LocaleUtils.serialize(Locale.CHINESE));
        when(queryParamsResolver.get()).thenReturn(queryParams);

        QueryParamsJsLocaleResolver jsLocaleResolver = new QueryParamsJsLocaleResolver(queryParamsResolver);

        assertEquals(Locale.CHINESE, jsLocaleResolver.getLocale());
    }

    @Test
    public void testEmptyLocale() {
        when(queryParams.get(QueryParamsJsLocaleResolver.QUERY_KEY)).thenReturn(null);
        when(queryParamsResolver.get()).thenReturn(queryParams);

        QueryParamsJsLocaleResolver jsLocaleResolver = new QueryParamsJsLocaleResolver(queryParamsResolver);

        assertEquals(Locale.US, jsLocaleResolver.getLocale());
    }
}
