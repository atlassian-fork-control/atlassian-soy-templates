package com.atlassian.soy.impl.modules;

import com.atlassian.soy.impl.functions.IncludeResourcesFunction;
import com.atlassian.soy.impl.functions.RequireResourceFunction;
import com.atlassian.soy.impl.functions.RequireResourcesForContextFunction;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import com.google.template.soy.shared.restricted.SoyFunction;

class WebResourceFunctionsModule extends AbstractModule {
    @Override
    protected void configure() {
        Multibinder<SoyFunction> binder = Multibinder.newSetBinder(binder(), SoyFunction.class);
        binder.addBinding().to(IncludeResourcesFunction.class);
        binder.addBinding().to(RequireResourceFunction.class);
        binder.addBinding().to(RequireResourcesForContextFunction.class);
    }
}
