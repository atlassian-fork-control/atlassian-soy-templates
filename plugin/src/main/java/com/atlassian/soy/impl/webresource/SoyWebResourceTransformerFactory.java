package com.atlassian.soy.impl.webresource;

import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.soy.impl.functions.LocaleUtils;
import com.atlassian.soy.impl.functions.UrlEncodingSoyFunctionSupplier;
import com.atlassian.webresource.api.prebake.DimensionAwareTransformerUrlBuilder;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.atlassian.soy.impl.i18n.QueryParamsJsLocaleResolver.QUERY_KEY;

/**
 * @since 2.4
 */
public class SoyWebResourceTransformerFactory implements DimensionAwareWebResourceTransformerFactory {
    private final SoyTransformerUrlBuilder soyTransformerUrlBuilder;
    private final SoyWebResourceTransformer soyWebResourceTransformer;
    private final WebResourceIntegration webResourceIntegration;
    private final UrlEncodingSoyFunctionSupplier soyFunctionSupplier;

    public SoyWebResourceTransformerFactory(SoyTransformerUrlBuilder soyTransformerUrlBuilder,
                                            SoyWebResourceTransformer soyWebResourceTransformer,
                                            WebResourceIntegration webResourceIntegration,
                                            UrlEncodingSoyFunctionSupplier soyFunctionSupplier) {
        this.soyTransformerUrlBuilder = soyTransformerUrlBuilder;
        this.soyWebResourceTransformer = soyWebResourceTransformer;
        this.webResourceIntegration = webResourceIntegration;
        this.soyFunctionSupplier = soyFunctionSupplier;
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters ignored) {
        return soyWebResourceTransformer;
    }

    @Override
    public Dimensions computeDimensions() {
        List<String> locales = StreamSupport.stream(webResourceIntegration.getSupportedLocales().spliterator(), false)
                .map(LocaleUtils::serialize).collect(Collectors.toList());
        return Dimensions.empty().andExactly(QUERY_KEY, locales)
                .product(soyFunctionSupplier.computeDimensions());
    }

    @Override
    public DimensionAwareTransformerUrlBuilder makeUrlBuilder(TransformerParameters ignored) {
        return soyTransformerUrlBuilder;
    }
}
