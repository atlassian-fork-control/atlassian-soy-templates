package com.atlassian.soy.impl.modules;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.spi.functions.SoyFunctionSupplier;
import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import com.atlassian.soy.spi.modules.GuiceModuleSupplier;
import com.atlassian.soy.spi.web.WebContextProvider;
import com.google.common.collect.ImmutableList;
import com.google.inject.Module;
import com.google.template.soy.data.SoyCustomValueConverter;

import java.util.Properties;

/**
 * Atlassian plugin specific implementation. Wires in some extra modules in addition to what DefaultGuiceModuleSupplier
 * does (specifically: things which enable pluggability).
 *
 * @since 2.3
 */
public class PluginsGuiceModuleSupplier implements GuiceModuleSupplier {
    private final Iterable<Module> modules;

    public PluginsGuiceModuleSupplier(
            SoyCustomValueConverter customValueConverter,
            I18nResolver i18nResolver,
            JsLocaleResolver jsLocaleResolver,
            Properties properties,
            SoyFunctionSupplier soyFunctionSupplier,
            WebContextProvider webContextProvider,
            WebResourceManager webResourceManager) {
        DefaultGuiceModuleSupplier defaultModules = new DefaultGuiceModuleSupplier(
                customValueConverter,
                i18nResolver,
                jsLocaleResolver,
                properties,
                soyFunctionSupplier,
                webContextProvider
        );

        this.modules = ImmutableList.<Module>builder()
                .addAll(defaultModules.get())
                .add(new PluginsBridgeModule(webResourceManager))
                .add(new WebResourceFunctionsModule())
                .build();
    }

    @Override
    public Iterable<Module> get() {
        return modules;
    }
}
