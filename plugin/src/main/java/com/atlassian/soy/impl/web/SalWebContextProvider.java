package com.atlassian.soy.impl.web;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.soy.spi.web.WebContextProvider;

import java.util.Locale;

/**
 * Simple implementation backed by a SAL HttpContext
 *
 * @since 2.3
 */
public class SalWebContextProvider implements WebContextProvider {
    private final LocaleResolver localeResolver;
    private final ApplicationProperties applicationProperties;

    public SalWebContextProvider(ApplicationProperties applicationProperties, LocaleResolver localeResolver) {
        this.applicationProperties = applicationProperties;
        this.localeResolver = localeResolver;
    }

    @Override
    public String getContextPath() {
        // UrlMode.RELATIVE preferentially uses the requests context path falling
        // back to the configured baseUrl in situations where a request context is not available
        return applicationProperties.getBaseUrl(UrlMode.RELATIVE);
    }

    @Override
    public Locale getLocale() {
        return localeResolver.getLocale();
    }
}
