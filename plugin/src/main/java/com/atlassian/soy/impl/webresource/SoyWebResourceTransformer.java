package com.atlassian.soy.impl.webresource;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.soy.impl.SoyManager;
import com.google.common.base.Supplier;

/**
 * @since 2.4
 */
public class SoyWebResourceTransformer implements UrlReadingWebResourceTransformer {
    private final SoyManager soyManager;
    private final ThreadLocalQueryParamsResolver queryParamsResolver;

    public SoyWebResourceTransformer(SoyManager soyManager, ThreadLocalQueryParamsResolver queryParamsResolver) {
        this.soyManager = soyManager;
        this.queryParamsResolver = queryParamsResolver;
    }

    @Override
    public DownloadableResource transform(TransformableResource transformableResource, QueryParams queryParams) {
        return new SoyDownloadableResource(
                transformableResource.nextResource(),
                transformableResource.location().getLocation(),
                queryParams
        );
    }

    private class SoyDownloadableResource extends CharSequenceDownloadableResource {

        private final String location;
        private final QueryParams queryParams;

        private SoyDownloadableResource(DownloadableResource nextResource, String location, QueryParams queryParams) {
            super(nextResource);
            this.location = location;
            this.queryParams = queryParams;
        }

        @Override
        protected CharSequence transform(final CharSequence originalContent) {
            return queryParamsResolver.withQueryParams(queryParams, new Supplier<CharSequence>() {
                @Override
                public CharSequence get() {
                    return soyManager.compile(originalContent, location);
                }
            });
        }
    }
}
